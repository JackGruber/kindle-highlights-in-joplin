#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""Synchronise Kindle eReader highlights and notes with Joplin

Imports and synchronises highlights and notes from a Kindle eReader to Joplin. \
This script creates a notebook in Joplin and creates individual notes for each \
book inside that notebook. All the highlights and annotations for each book are \
stored in the same note and separated by a horizontal line. When a note for a book \
already exists, this script updates the note with the new highlights.

This module requires that `joplin_api`, `httpx`, `dateparser` and `pandas` be \
installed within the Python environment you are running this script in. 
See https://gitlab.com/annyong/joplin-api 

To use this script, modify first the `conf.py` file in which you must specify \
your token, the name of the notebook in Joplin to store your highlights and \
the path to the file in which the highlights are stored.

Details
---------

* Kindle eReaders usually store the highlights and notes in a txt file. The name \
of the file depends on the language. In English the name is 'My Clippings.txt', \
whereas in spanish it is 'Mis recortes.txt'. 

* The elements stored in that file follow a specific structure, a specific \
sequence of characters separates individual highlights. The metadata for \
each highlight is written in different languages according to the device configuration.
This package uses regular expressions to retrieve the metadata to build a unique \
identifier for each note. So far, it includes keywords in Spanish and English to \
identify the relevant data. You can add keywords for a different language in the \
`kindle_conf` module. 

* So far, this script has only been tested with the Clipping files from a \
Kindle D01100 device. 


@author: @seawind
"""
from conf import token, folder, path_highlights
from hl_tools import kindle_extr
from hl_tools import joplin_lib
from hl_tools import build_note




joplin_lib.create_folder_if_not_exists(token,folder)

highlights = kindle_extr.get_highlights(path_highlights)
## Combines the title and the author and gets rid of the quotation marks in the titles
highlights['book'] = highlights[['title', 'author']].agg(' - '.join, axis=1).str.replace('"', "'", regex=False)
highlights['identifier'] = highlights[['type_hl','date_time','location','page']].apply(build_note.create_identifier,axis=1)

titles = kindle_extr.get_book_and_author(highlights)

created_notes = 0
updated_notes = 0
non_updated_notes = 0
for title in titles:
    hl_book = highlights[highlights['book']==title]
    hl_book = hl_book.sort_values(by='date_time',ascending=True)
    ### Looks for an existing note for that title    
    sear_note = joplin_lib.search_note(token,folder,title)
    if sear_note['note_id'] != '': 
        ## If the note exists, it is retrieved
        
        note = joplin_lib.get_note(token,sear_note['note_id'])     
        missing_hls = build_note.find_missing_highlights(hl_book,note['body'])
        if len(missing_hls) == 0:
            non_updated_notes += 1
        else:
            previous_body = note['body']
            new_body = build_note.create_note_body(hl_book,title,missing_hls)
            body = previous_body + new_body            
            status = joplin_lib.update_note(token,sear_note['note_id'],title,body,sear_note['nb_id'])
            if status.status_code == 200:
                print(f'{len(missing_hls)} highlights were updated successfully:\n\t'+f'{title}')
                updated_notes += 1            
    else:       
        ### If the note does not exist
        missing_hls = list(hl_book.index)
        body = build_note.create_note_body(hl_book,title,missing_hls)
        
        status = joplin_lib.create_note(token,title,body,sear_note['nb_id'])
        if status.status_code == 200:
            print(f'Note with {len(hl_book)} highlights created successfully:\n\t'+f'{title}')
            created_notes += 1
print(f'{created_notes} notes created.')
print(f'{updated_notes} notes updated.')
print(f'{non_updated_notes} notes not updated.')


